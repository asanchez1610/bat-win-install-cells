@echo off
setlocal enabledelayedexpansion

rem Obtiene el PATH del usuario actual
for /f "tokens=2*" %%A in ('reg query "HKCU\Environment" /v PATH 2^>nul') do set "USER_PATH=%%B"

rem Verifica si la variable USER_PATH está vacía
if "%USER_PATH%"=="" (
    echo No se encontró la variable PATH del usuario.
    exit /b 1
)

rem Muestra el PATH original
echo PATH original: %USER_PATH%

rem Inicializa la nueva variable PATH
set "NEW_PATH="

rem Bucle para filtrar las rutas que contengan "node" o "npm"
for %%i in ("%USER_PATH:;=";"%") do (
    set "ROUTE=%%~i"
    if "!ROUTE:node=!"=="!ROUTE!" (
        if "!ROUTE:npm=!"=="!ROUTE!" (
            if defined NEW_PATH (
                set "NEW_PATH=!NEW_PATH!;!ROUTE!"
            ) else (
                set "NEW_PATH=!ROUTE!"
            )
        )
    )
)

rem Muestra el nuevo PATH filtrado
echo PATH filtrado: %NEW_PATH%

rem Actualiza el PATH del usuario en el registro
reg add "HKCU\Environment" /v PATH /t REG_EXPAND_SZ /d "%NEW_PATH%" /f

echo PATH del usuario actualizado.
endlocal
