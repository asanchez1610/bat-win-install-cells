@echo off
set "OLD_PATH=%PATH%"

set "PATH=%1;%PATH%"

npm config set registry https://artifactory.globaldevtools.bbva.com:443/artifactory/api/npm/gl-bbva-npm-virtual

set "PATH=%OLD_PATH%"

set "OLD_PATH="