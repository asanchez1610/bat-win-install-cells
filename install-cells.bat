@echo off
set "OLD_PATH=%PATH%"

set "PATH=%1;%PATH%"

set version=%2

rem Verificar si cells-cli está instalado
cells -v > nul 2>&1
if %errorlevel% equ 0 (
    echo cells-cli ya está instalado, desinstalándolo...
    npm uninstall -g @cells/cells-cli
)

if "%version%"=="" (
    npm install -g @cells/cells-cli
) else (
    npm install -g @cells/cells-cli@%version%
)

set "PATH=%OLD_PATH%"

set "OLD_PATH="
