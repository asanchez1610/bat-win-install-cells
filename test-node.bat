@echo off
set "OLD_PATH=%PATH%"

set "PATH=%1;%PATH%"

if "%2"=="npm" (
    npm -v
) else if "%2"=="yarn" (
    yarn -v
) else if "%2"=="bower" (
    bower -v
) else if "%2"=="cells" (
    cells -v
) else (
    node -v
)

set "PATH=%OLD_PATH%"
set "OLD_PATH="