@echo off
set "OLD_PATH=%PATH%"

set "PATH=%1;%PATH%"

npm i -g yarn

set "PATH=%OLD_PATH%"

set "OLD_PATH="